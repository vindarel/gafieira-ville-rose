---
title: "Accueil"
order: 0
in_menu: true
---
# Gafieira Ville Rose

Nous sommes des danseuses et danseurs de forró, tango, trad, swing… qui aimons l'énergie de la gafieira. Nous souhaitons la développer à Toulouse. Venez nous rejoindre et n'hésitez pas à venir découvrir !

### Actualités

Début mars, nous avons le plaisir d'accueillir Robinho & Evelin, deux professeurs exceptionnel·les venu·es tout droit de Rio de Janeiro pour un week-end de stages intensifs !

### Nos cours

Nous proposons des cours débutants les mercredi à 20h à la Candela (St Cyprien, Toulouse) et des cours intermédiaires un samedi par mois. 

### Nous suivre

Nous sommes sur les réseaux !

- Instagram: https://www.instagram.com/gafieira_villerose/
- Telegram: https://t.me/gafieirataboa
- Facebook: Gafieira Ville Rose

### Mais qu'est-ce que la samba de gafieira ?

La samba de gafieira est une danse de couple brésilienne. On y retrouve une
posture et des jeux de jambes proches du tango, mais… avec un swing tout brésilien. 